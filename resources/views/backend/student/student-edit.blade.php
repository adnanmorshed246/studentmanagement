@extends('layouts.backend.master')

@section('title','Add Student Information Data')

@section('content')
<section class=""> 
<section class="card ">
@include('messages.message')
    <header class="card-header">
        Forms Wizard
    </header>
    <div class="card-body">
        
        <form  id="default" action="{{url('student/'.$student->id)}}" method="post" enctype="multipart/form-data" >
            @csrf
            <div class="form-row ">
                <div class="form-group col-lg-4">
                    <label class="control-label">First Name</label>
                    <div class="">
                        <input type="text" class="form-control" placeholder="First Name" name="f_name" value="{{$student->f_name}}">
                    </div>
                </div>
                <div class="form-group col-lg-4">
                    <label class=" control-label">Last Name</label>
                    <div class="">
                        <input type="text" class="form-control" placeholder="Last Name" name="l_name" value="{{$student->l_name}}">
                    </div>
                </div>
                <div class="form-group col-lg-4">
                    <label class=" control-label">User Name</label>
                    <div class="">
                        <input type="text" class="form-control" placeholder="Username" name="u_name"  value="{{$student->u_name}}">
                    </div>
                </div>
            </div>

            <div class="form-row ">
                <div class="form-group col-lg-4">
                    <label class="control-label">Mobile Number</label>
                    <div class="">
                        <input type="text" class="form-control" placeholder="Mobile Number" name="mobile"  value="{{$student->mobile}}">
                    </div>
                </div>
                <div class="form-group col-lg-4">
                    <label class=" control-label">Email</label>
                    <div class="">
                        <input type="email" class="form-control" placeholder="Write Email.." name="email"  value="{{$student->email}}">
                    </div>
                </div>
                <div class="form-group col-lg-4" >
                    <label class=" control-label"> Department Name</label>
                    <div class="">
                        <select name="departments_id" id="" class="col-12 p-2 border">
                            @foreach ($department as $key => $value)
                            @if ($value->id == $student->departments_id)
                            <option value="{{$value->id}}" selected>{{$value->dpt_name}} </option>
                            @else
                            <option value="{{$value->id}}">{{$value->dpt_name}} </option>
                            @endif
                            
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-row ">
                <div class="form-group col-lg-3">
                    <label class="control-label">Select Photo</label>
                    <div class="">
                        <input type="file" class="form-control" placeholder="Select Photo" name="image">
                    </div>
                    
                </div>
                <div class="photo col-lg-2 ">
                    <img class="img img-fluid " style="height:100px" src="{{asset('images/'.$student->image)}}" alt="">
                </div>
                <div class="form-group col-lg-4">
                    <label class=" control-label">City</label>
                    <div class="">
                        <select name="city" id="" class="col-12 p-2 border">
                           
                                <option value="{{$student->city}}">{{$student->city}}</option>
                                <option value="Dhaka">Dhaka</option>
                                <option value="Chattagram">Chattagram</option>
                                <option value="Khulna">Khulna</option>
                                <option value="Rajsahi">Rajsahi</option>
                                <option value="Barisal">Barisal</option>
                                <option value="Rangpur">Rangpur</option>
                                <option value="Commilla">Commilla</option>
                                <option value="Maymansing">Maymansing</option>
                            
                            
                        </select>
                    </div>
                </div>
                
            </div>
            <button type="submit" class="btn btn-success" > Update Data</button>
               
        </form>
    </div>
</section>
</section>

@endsection