@extends('layouts.backend.master')

@section('title','Add Student Information Data')

@section('content')
<section class=""> 
<section class="card ">
@include('messages.message')
    <header class="card-header">
        Forms Wizard
    </header>
    <div class="card-body">
        
        <form  id="default" action="{{url('students/')}}" method="post" enctype="multipart/form-data" >
            @csrf
            <div class="form-row ">
                <div class="form-group col-lg-4">
                    <label class="control-label">First Name</label>
                    <div class="">
                        <input type="text" class="form-control" placeholder="First Name" name="f_name">
                    </div>
                </div>
                <div class="form-group col-lg-4">
                    <label class=" control-label">Last Name</label>
                    <div class="">
                        <input type="text" class="form-control" placeholder="Last Name" name="l_name">
                    </div>
                </div>
                <div class="form-group col-lg-4">
                    <label class=" control-label">User Name</label>
                    <div class="">
                        <input type="text" class="form-control" placeholder="Username" name="u_name">
                    </div>
                </div>
            </div>

            <div class="form-row ">
                <div class="form-group col-lg-4">
                    <label class="control-label">Mobile Number</label>
                    <div class="">
                        <input type="text" class="form-control" placeholder="Mobile Number" name="mobile">
                    </div>
                </div>
                <div class="form-group col-lg-4">
                    <label class=" control-label">Email</label>
                    <div class="">
                        <input type="email" class="form-control" placeholder="Write Email.." name="email">
                    </div>
                </div>
                <div class="form-group col-lg-4">
                    <label class=" control-label">Password </label>
                    <div class="">
                        <input type="password" class="form-control" placeholder="password" name="password">
                    </div>
                </div>
            </div>

            <div class="form-row ">
                <div class="form-group col-lg-4">
                    <label class="control-label">Select Photo</label>
                    <div class="">
                        <input type="file" class="form-control" placeholder="Select Photo" name="image">
                    </div>
                </div>
                <div class="form-group col-lg-4">
                    <label class=" control-label">City</label>
                    <div class="">
                        <select name="city" id="" class="col-12 p-2 border">
                            <option value="">Select Your Divition</option>
                            <option value="Dhaka">Dhaka</option>
                            <option value="Chattagram">Chattagram</option>
                            <option value="Khulna">Khulna</option>
                            <option value="Rajsahi">Rajsahi</option>
                            <option value="Barisal">Barisal</option>
                            <option value="Rangpur">Rangpur</option>
                            <option value="Commilla">Commilla</option>
                            <option value="Maymansing">Maymansing</option>
                        </select>
                    </div>
                </div>
                <div class="form-group col-lg-4" >
                    <label class=" control-label"> Department Name</label>
                    <div class="">
                        <select name="departments_id" id="" class="col-12 p-2 border">
                            <option value="">Select Your Department </option>
                            @foreach ($data as $value)
                            <option value="{{$value->id}}">{{$value->dpt_name}} </option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-success" > Save Data</button>
               
        </form>
    </div>
</section>
</section>

@endsection