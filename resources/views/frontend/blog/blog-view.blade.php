@extends('layouts.frontend.master')
@section('title','Blog Post')
@section('content')
<!--====================  breadcrumb area ====================-->
<div class="breadcrumb-area breadcrumb-area-bg section-space--inner--80 bg-img" data-bg="{{asset('frontend/assets/img/backgrounds/19.jpg')}}">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-sm-6">
                <h2 class="breadcrumb-page-title">Blog List</h2>
            </div>
            <div class="col-sm-6">
                <ul class="breadcrumb-page-list text-uppercase">
                    <li class="has-children"><a href="index.html">Home</a></li>
                    <li>Blog List</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--====================  End of breadcrumb area  ====================-->
<!--====================  page content wrapper ====================-->
<div class="page-content-wrapper section-space--inner--120">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <!-- blog content wrapper -->
                <div class="blog-content-wrapper">
                    <div class="single-list-blog-post-wrapper">
                        <div class="single-list-blog-post">
                            <div class="single-list-blog-post__image section-space--bottom--50">
                                <a href="blog-post-image.html">
                                    <img src="assets/img/blog/1-1170x684.jpg" class="img-fluid" alt="">
                                </a>
                            </div>
                            <div class="single-list-blog-post__content">
                                <ul class="tag-list">
                                    <li><a href="#">Material Engineering</a></li>
                                    <li><a href="#">Delivery</a></li>
                                </ul>
                                <h1 class="title"> <a href="blog-post-image.html">Commercial Ministry to Hike Import Duty on Aluminium</a></h1>
                                <div class="meta-list">
                                    <div class="single-meta">
                                        <p class="date">AUGUST 4, 2019</p>
                                    </div>
                                    <div class="single-meta">
                                        <p class="author">BY <a href="#">EAGER-KOWALEVSKI</a></p>
                                    </div>
                                    <div class="single-meta">
                                        <p class="comment-info"><a href="#">0 COMMENTS</a></p>
                                    </div>
                                </div>
                                <div class="post-excerpt">
                                    New Delhi: India has challenged the WTO dispute panel's ruling that the country's move to impose safeguard duty on some iron and steel products was inconsistent with certain global trade norms, an official said. The appellate body and the panel are part of the World Trade Organisation's (WTO) dispute settlement mechanism. It is a 164-member multilateral body which makes rules related to global exports and imports. …
                                </div>
                                <a href="blog-post-image.html" class="ht-btn ht-btn--default">READ MORE</a>
                            </div>
                        </div>
                        <div class="single-list-blog-post">
                            <div class="single-list-blog-post__image section-space--bottom--50">
                                <a href="blog-post-image.html">
                                    <img src="assets/img/blog/2-1170x684.jpg" class="img-fluid" alt="">
                                </a>
                            </div>
                            <div class="single-list-blog-post__content">
                                <ul class="tag-list">
                                    <li><a href="#">Material Engineering</a></li>
                                    <li><a href="#">Delivery</a></li>
                                </ul>
                                <h1 class="title"> <a href="blog-post-image.html">Steels’ Safety: India Appeals Against WTO’s Panel Ruling</a></h1>
                                <div class="meta-list">
                                    <div class="single-meta">
                                        <p class="date">AUGUST 4, 2019</p>
                                    </div>
                                    <div class="single-meta">
                                        <p class="author">BY <a href="#">EAGER-KOWALEVSKI</a></p>
                                    </div>
                                    <div class="single-meta">
                                        <p class="comment-info"><a href="#">0 COMMENTS</a></p>
                                    </div>
                                </div>
                                <div class="post-excerpt">
                                    New Delhi: India has challenged the WTO dispute panel's ruling that the country's move to impose safeguard duty on some iron and steel products was inconsistent with certain global trade norms, an official said. The appellate body and the panel are part of the World Trade Organisation's (WTO) dispute settlement mechanism. It is a 164-member multilateral body which makes rules related to global exports and imports. …
                                </div>
                                <a href="blog-post-image.html" class="ht-btn ht-btn--default">READ MORE</a>
                            </div>
                        </div>
                        <div class="single-list-blog-post">
                            <div class="single-list-blog-post__image section-space--bottom--50">
                                <a href="blog-post-image.html">
                                    <img src="assets/img/blog/3-1170x684.jpg" class="img-fluid" alt="">
                                </a>
                            </div>
                            <div class="single-list-blog-post__content">
                                <ul class="tag-list">
                                    <li><a href="#">Material Engineering</a></li>
                                    <li><a href="#">Delivery</a></li>
                                </ul>
                                <h1 class="title"> <a href="blog-post-image.html">Default Interest Rate in Small Loans Now is Lowest</a></h1>
                                <div class="meta-list">
                                    <div class="single-meta">
                                        <p class="date">AUGUST 4, 2019</p>
                                    </div>
                                    <div class="single-meta">
                                        <p class="author">BY <a href="#">EAGER-KOWALEVSKI</a></p>
                                    </div>
                                    <div class="single-meta">
                                        <p class="comment-info"><a href="#">0 COMMENTS</a></p>
                                    </div>
                                </div>
                                <div class="post-excerpt">
                                    New Delhi: India has challenged the WTO dispute panel's ruling that the country's move to impose safeguard duty on some iron and steel products was inconsistent with certain global trade norms, an official said. The appellate body and the panel are part of the World Trade Organisation's (WTO) dispute settlement mechanism. It is a 164-member multilateral body which makes rules related to global exports and imports. …
                                </div>
                                <a href="blog-post-image.html" class="ht-btn ht-btn--default">READ MORE</a>
                            </div>
                        </div>
                        <div class="single-list-blog-post">
                            <div class="single-list-blog-post__image section-space--bottom--50">
                                <a href="blog-post-image.html">
                                    <img src="assets/img/blog/4-1170x684.jpg" class="img-fluid" alt="">
                                </a>
                            </div>
                            <div class="single-list-blog-post__content">
                                <ul class="tag-list">
                                    <li><a href="#">Material Engineering</a></li>
                                    <li><a href="#">Delivery</a></li>
                                </ul>
                                <h1 class="title"> <a href="blog-post-image.html">Why Trump is so clumsy about fighting for ‘Free Trade’</a></h1>
                                <div class="meta-list">
                                    <div class="single-meta">
                                        <p class="date">AUGUST 4, 2019</p>
                                    </div>
                                    <div class="single-meta">
                                        <p class="author">BY <a href="#">EAGER-KOWALEVSKI</a></p>
                                    </div>
                                    <div class="single-meta">
                                        <p class="comment-info"><a href="#">0 COMMENTS</a></p>
                                    </div>
                                </div>
                                <div class="post-excerpt">
                                    New Delhi: India has challenged the WTO dispute panel's ruling that the country's move to impose safeguard duty on some iron and steel products was inconsistent with certain global trade norms, an official said. The appellate body and the panel are part of the World Trade Organisation's (WTO) dispute settlement mechanism. It is a 164-member multilateral body which makes rules related to global exports and imports. …
                                </div>
                                <a href="blog-post-image.html" class="ht-btn ht-btn--default">READ MORE</a>
                            </div>
                        </div>

                    </div>
                </div>

                <!--====================  pagination ====================-->

                <div class="pagination-wrapper section-space--inner--top--40">
                    <ul class="page-pagination">
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">NEXT</a></li>
                    </ul>
                </div>

                <!--====================  End of pagination ====================-->
            </div>
            <div class="col-lg-4">
                <!-- page sidebar -->
                <div class="page-sidebar">
                    <!-- single sidebar widget -->
                    <div class="single-sidebar-widget">
                        <h2 class="widget-title">Search</h2>
                        <div class="sidebar-search">
                            <form action="#">
                                <input type="text" placeholder="Keyword search ...">
                                <button type="submit"><i class="ion-ios-search-strong"></i></button>
                            </form>
                        </div>
                    </div>
                    <!-- single sidebar widget -->
                    <div class="single-sidebar-widget">
                        <h2 class="widget-title">Categories</h2>
                        <ul class="sidebar-category">
                            <li><a href="blog-list.html">Delivery <span class="counter">4</span></a></li>
                            <li><a href="blog-list.html">Material Engineering <span class="counter">3</span></a></li>
                            <li><a href="blog-list.html">Oil & gas <span class="counter">1</span></a></li>
                            <li><a href="blog-list.html">Power and Energy <span class="counter">2</span></a></li>
                        </ul>
                    </div>
                    <!-- single sidebar widget -->
                    <div class="single-sidebar-widget">
                        <h2 class="widget-title">Recent News</h2>
                        <div class="sidebar-recent-post-list">
                            <div class="sidebar-recent-post">
                                <div class="sidebar-recent-post__image">
                                    <a href="blog-post-image.html">
                                        <img src="{{asset('frontend/assets/img/blog/1-80x80.jpg')}}" class="img-fluid" alt="">
                                    </a>
                                </div>
                                <div class="sidebar-recent-post__content">
                                    <h3 class="title">
                                        <a href="blog-post-image.html">Commercial Ministry to Hike Import Duty on Aluminium</a>
                                    </h3>
                                    <p class="post-date">AUGUST 04, 2019</p>
                                </div>
                            </div>
                            <div class="sidebar-recent-post">
                                <div class="sidebar-recent-post__image">
                                    <a href="blog-post-image.html">
                                        <img src="assets/img/blog/2-80x80.jpg" class="img-fluid" alt="">
                                    </a>
                                </div>
                                <div class="sidebar-recent-post__content">
                                    <h3 class="title">
                                        <a href="blog-post-image.html">Steels’ Safety: India Appeals Against WTO’s Panel Ruling</a>
                                    </h3>
                                    <p class="post-date">AUGUST 04, 2019</p>
                                </div>
                            </div>
                            <div class="sidebar-recent-post">
                                <div class="sidebar-recent-post__image">
                                    <a href="blog-post-image.html">
                                        <img src="assets/img/blog/3-80x80.jpg" class="img-fluid" alt="">
                                    </a>
                                </div>
                                <div class="sidebar-recent-post__content">
                                    <h3 class="title">
                                        <a href="blog-post-image.html">Default Interest Rate in Small Loans Now is Lowest</a>
                                    </h3>
                                    <p class="post-date">AUGUST 04, 2019</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- single sidebar widget -->
                    <div class="single-sidebar-widget">
                        <h2 class="widget-title">Tag Cloud</h2>
                        <ul class="sidebar-tag-list">
                            <li><a href="#">Engineering (6)</a></li>
                            <li><a href="#">Transport (6)</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--====================  End of page content wrapper  ====================-->

@endsection