<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Students extends Model
{
    protected $fillable = ['f_name','l_name','u_name','mobile','email','password','image','city','departments_id'] ;
}
