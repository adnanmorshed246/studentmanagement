<?php

namespace App\Http\Controllers;

use App\Department;
use App\Students;
use Illuminate\Http\Request;
use Image;
use DB;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Students::join('departments','students.departments_id','=','departments.id')->select('students.*','departments.dpt_name')->get();

        // $data = Students::join('students','students.departments_id','=','departments.id')->select('students.*','departments.dpt_name')->get();

        return view('backend.student.student-view',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = Department::all();
        return view('backend.student.student-create',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Students;
        if($request->hasfile('image')){
            $image = $request->file('image');
            $file_name = time().'.'.$image->getClientOriginalExtension();
            $image_resize = Image::make($image->getRealPath());
            $image_resize->resize(300,300);
            $image_resize->save('images/'.$file_name);
            // $request->image->move('images/',$file_name);
            $data->image=$file_name;
            
        }
        $data->f_name = $request->f_name;
        $data->l_name = $request->l_name;
        $data->u_name = $request->u_name;
        $data->mobile = $request->mobile;
        $data->email = $request->email;
        $data->password = $request->password;
        
        $data->city = $request->city;
        $data->departments_id = $request->departments_id;
        
        $data->save();
        return back()->with('success','Add Student Data Has been Successfully Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $student = Students::find($id);
        
        // $data = Department::join('Students','departments.id','=','students.departments_id')->select('departments.*','students.id')->get();
        // $department = DB::table('students')->join('departments','students.departments_id','departments.id')->get();

        $department = Department::all();
        // $student_data = Students::join('departments','students.departments_id','=','departments.id')->select('students.*','departments.dpt_name')->where('id',$id);

        // join('students','students.departments_id','=','departments.id')->select('students.*','departments.dpt_name');
        return view('backend.student.student-edit',compact('student','department'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $data = Students::find($id);
       if($request->hasFile('image')){
           $image = $request->file('image');
           $file_name = time().'.'.$image->getClientOriginalExtension();
           // remove previous image file 
           $old_image = $data->image;
           if( $old_image != ""){
               $path = "images/$old_image";
               unlink($path);
           }
           
           
           $image_resize = Image::make($image->getRealPath());
           $image_resize->resize(300,300);
           
           $image_resize->save('images/'.$file_name);
           
           $data->image = $file_name;
           
       }
        $data->f_name = $request->f_name;
        $data->l_name = $request->l_name;
        $data->u_name = $request->u_name;
        $data->mobile = $request->mobile;
        $data->email = $request->email;
        $data->city = $request->city;
        $data->departments_id = $request->departments_id;
        $data->save();
        return redirect('view/student-view')->with('success',"Student Data Update Successfully !!!!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $data = Students::find($id);
       
       $old_image = $data->image;
        if( $old_image != ""){
            $path = "images/$old_image";
            unlink($path);
        }
        $data->delete();
        return back()->with('success','Student Data Has Been Success fully Deleted ');
    }
}
