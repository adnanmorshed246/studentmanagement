<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    protected $fillable = ['category_id','title','description','post_image','tag'];
}
