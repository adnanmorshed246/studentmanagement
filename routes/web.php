<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// forntend Default Route 
Route::get('/', function () {
    return view('pages.index');
});
// Backend Default Route 
Route::get('/admin', function () {
    return view('pages.backend.index');
});

// Route Department Data

Route::get('layouts/add-department','DepartmentController@create');
Route::post('department','DepartmentController@store');
Route::get('view/department-view','DepartmentController@index');
Route::get('department/{id}/delete','DepartmentController@destroy');
Route::get('department/{id}/edit','DepartmentController@edit');
Route::post('department/{id}','DepartmentController@update');

// Route Student Data 
Route::get('layouts/add-student','StudentController@create');
Route::post('students','StudentController@store');
Route::get('view/student-view','StudentController@index');
Route::get('student/{id}/edit','StudentController@edit');
Route::post('student/{id}','StudentController@update');
Route::get('student/{id}/delete','StudentController@destroy');

// registration
Route::get('register','RegistrationController@create');
Route::post('sign-up','RegistrationController@register');

Route::get('login','LoginController@loginform');
Route::get('logout','LoginController@logout');
Route::post('login-form','LoginController@login');

// Route for Category 
Route::get('post/add-category','Admin\CategoryController@create');
Route::get('view/category-view','Admin\CategoryController@index');
Route::post('category','admin\CategoryController@store');
Route::delete('category/{id}/delete','Admin\CategoryController@destroy');
Route::get('category/{id}/edit','Admin\CategoryController@edit');
Route::post('category/{id}/','Admin\CategoryController@update');

// Route For Post 
Route::get('post/add-post','Admin\PostController@create');
Route::post('post','Admin\PostController@store');


// Route Frontend 
Route::get('blog-post','Frontend\BlogController@view');